package com.MoofIT.Minecraft.Cenotaph;

/**
 * Cenotaph - A Dead Man's Chest plugin for Bukkit
 * By Jim Drey (Southpaw018) <moof@moofit.com>
 * Original Copyright (C) 2011 Steven "Drakia" Scott <Drakia@Gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapAPI;


import org.yi.acru.bukkit.Lockette.Lockette;


public class Cenotaph extends JavaPlugin {
	public final CenotaphEntityListener entityListener = new CenotaphEntityListener(this);
	public final CenotaphBlockListener blockListener = new CenotaphBlockListener(this);
	public final CenotaphPlayerListener playerListener = new CenotaphPlayerListener(this);
	public final CenotaphCommand commandExec = new CenotaphCommand(this);
	public final DynmapThread dynThread = new DynmapThread(this);
	public static final Logger log = Logger.getLogger("Minecraft");
	PluginManager pm;

	public Lockette LockettePlugin = null;
	public DynmapAPI dynmap = null;

	public static ConcurrentLinkedQueue<TombBlock> tombList = new ConcurrentLinkedQueue<>();
	public static HashMap<Location, TombBlock> tombBlockList = new HashMap<>();
	public static HashMap<String, ArrayList<TombBlock>> playerTombList = new HashMap<>();
	public static HashMap<String, EntityDamageEvent> deathCause = new HashMap<>();

	public FileConfiguration config;

	/**
	 * Configuration options - Defaults
	 */
	//Core
	public boolean cenotaphSign = true;
	public boolean noDestroy = true;
	public boolean saveCenotaphList = true;
	public boolean noInterfere = true;
	public boolean voidCheck = true;
	public boolean creeperProtection = false;
	public String signMessage[] = new String[] {
		"{name}",
		"DEP",
		"{date}",
		"{time}"
	};
	public String dateFormat = "MM/dd/yyyy";
	public String timeFormat = "hh:mm a";
	public List<String> desactivadoEnMundos;
	public boolean dynmapEnable = true;

	//Removal
	public boolean destroyQuickLoot = true;
	public boolean cenotaphRemove = true;
	public int removeTime = 3600;
	public boolean removeWhenEmpty = false;
	public boolean keepUntilEmpty = false;
	public boolean levelBasedRemoval = false;
	public int levelBasedTime = 60;

	//Security
	public boolean LocketteEnable = true;
	public boolean securityRemove = false;
	public int securityTimeout = 3600;

	//DeathMessages
	public HashMap<String, Object> deathMessages = new HashMap<>();

	//Config versioning
	public int configVer = 0;
	public final int configCurrent = 16;

	@Override
	public void onEnable() {
		log.log(Level.INFO, "Cenotaph {0} ha sido activado.", getDescription().getVersion());

		pm = getServer().getPluginManager();

		pm.registerEvents(entityListener,this);
		pm.registerEvents(blockListener,this);
		pm.registerEvents(playerListener,this);

		LockettePlugin = (Lockette)loadPlugin("Lockette");
		dynmap = (DynmapAPI)loadPlugin("dynmap");

		initDeathMessagesDefaults();
		loadConfig();
		if (dynmapEnable && dynmap != null) dynThread.activate(dynmap);
		for (World w : getServer().getWorlds())
			loadTombList(w.getName());

		// Start removal timer. Run every 5 seconds (20 ticks per second)
		if (securityRemove || cenotaphRemove)
			getServer().getScheduler().scheduleSyncRepeatingTask(this, new TombThread(this), 0L, 100L);
	}

	public void loadConfig() {
		this.reloadConfig();
		config = this.getConfig();

		configVer = config.getInt("configVer", configVer);
		if (configVer == 0) {
			log.info("[Cenotaph] Configuration error or no config file found. Generating default config file.");
			saveDefaultConfig();
			this.reloadConfig();
			config = this.getConfig();
		}
		else if (configVer < configCurrent) {
			log.warning("[Cenotaph] Your config file is out of date! Delete your config and /cenadmin reload to see the new options. Proceeding using set options from config file and defaults for new options..." );
		}

		try {
			desactivadoEnMundos = config.getStringList("Cenotaph.desactivadoEnMundo");
		} catch (NullPointerException e) {
			log.warning("[Cenotaph] Usando defaults en configuración de mundos.");
		}
	}

	private void initDeathMessagesDefaults() {
		deathMessages.put("Monster.Zombie", "a Zombie");
		deathMessages.put("Monster.Skeleton", "a Skeleton");
		deathMessages.put("Monster.Spider", "a Spider");
		deathMessages.put("Monster.Wolf", "a Wolf");
		deathMessages.put("Monster.Creeper", "a Creeper");
		deathMessages.put("Monster.Slime", "a Slime");
		deathMessages.put("Monster.Ghast", "a Ghast");
		deathMessages.put("Monster.PigZombie", "a Pig Zombie");
		deathMessages.put("Monster.Giant", "a Giant");
		deathMessages.put("Monster.Other", "a Monster");
		deathMessages.put("Monster.Blaze", "a Blaze");
		deathMessages.put("Monster.CaveSpider", "a Cave Spider");
		deathMessages.put("Monster.EnderDragon", "a Dragon");
		deathMessages.put("Monster.Enderman", "an Enderman");
		deathMessages.put("Monster.IronGolem", "an Iron Golem");
		deathMessages.put("Monster.MagmaCube", "a Magma Cube");
		deathMessages.put("Monster.Silverfish", "a Siverfish");

		deathMessages.put("World.Cactus", "a Cactus");
		deathMessages.put("World.Suffocation", "Suffocation");
		deathMessages.put("World.Fall", "a Fall");
		deathMessages.put("World.Fire", "a Fire");
		deathMessages.put("World.Burning", "Burning");
		deathMessages.put("World.Lava", "Lava");
		deathMessages.put("World.Drowning", "Drowning");
		deathMessages.put("World.Lightning", "Lightning");

		deathMessages.put("Explosion.Misc", "an Explosion");
		deathMessages.put("Explosion.TNT", "a TNT Explosion");

		deathMessages.put("Misc.Dispenser", "a Dispenser");
		deathMessages.put("Misc.Void", "the Void");
		deathMessages.put("Misc.Other", "Unknown");
	}
	

	public void loadTombList(String world) {
		if (!saveCenotaphList) return;
		try {
			File fh = new File(this.getDataFolder().getPath(), "tombList-" + world + ".db");
			if (!fh.exists()) return;
			Scanner scanner = new Scanner(fh);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				String[] split = line.split(":");
				Block block = readBlock(split[0]);
				Block lBlock = readBlock(split[1]);
				Block sign = readBlock(split[2]);
				String owner = split[3];
				int level = Integer.valueOf(split[4]);
				long time = Long.valueOf(split[5]);


				if (block == null || owner == null) {
					log.log(Level.INFO, "[Cenotaph] Invalid entry in database {0}", fh.getName());
					continue;
				}
				TombBlock tBlock = new TombBlock(block, lBlock, sign, owner, level, time);
				tombList.offer(tBlock);
				// Used for quick tombStone lookup
				tombBlockList.put(block.getLocation(), tBlock);
				if (lBlock != null) tombBlockList.put(lBlock.getLocation(), tBlock);
				if (sign != null) tombBlockList.put(sign.getLocation(), tBlock);
				ArrayList<TombBlock> pList = playerTombList.get(owner);
				if (pList == null) {
					pList = new ArrayList<>();
					playerTombList.put(owner, pList);
				}
				pList.add(tBlock);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			log.log(Level.INFO, "[Cenotaph] Error loading cenotaph list: {0}", e);
		}
	}

	public void saveCenotaphList(String world) {
		if (!saveCenotaphList) return;
		try {
			File fh = new File(this.getDataFolder().getPath(), "tombList-" + world + ".db");
			BufferedWriter bw = new BufferedWriter(new FileWriter(fh));
			for (Iterator<TombBlock> iter = tombList.iterator(); iter.hasNext();) {
				TombBlock tBlock = iter.next();
				// Skip not this world
				if (!tBlock.getBlock().getWorld().getName().equalsIgnoreCase(world)) continue;

				StringBuilder builder = new StringBuilder();

				bw.append(printBlock(tBlock.getBlock()));
				bw.append(":");
				bw.append(printBlock(tBlock.getLBlock()));
				bw.append(":");
				bw.append(printBlock(tBlock.getSign()));
				bw.append(":");
				bw.append(tBlock.getOwner());
				bw.append(":");
				bw.append(Integer.toString(tBlock.getOwnerLevel()));
				bw.append(":");
				bw.append(String.valueOf(tBlock.getTime()));

				bw.append(builder.toString());
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			log.log(Level.INFO, "[Cenotaph] Error saving cenotaph list: {0}", e);
		}
	}

	private String printBlock(Block b) {
		if (b == null) return "";
		return b.getWorld().getName() + "," + b.getX() + "," + b.getY() + "," + b.getZ();
	}

	private Block readBlock(String b) {
		if (b.length() == 0) return null;
		String[] split = b.split(",");
		//world,x,y,z
		World world = getServer().getWorld(split[0]);
		if (world == null) return null;
		return world.getBlockAt(Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));
	}

	@Override
	public void onDisable() {
		for (World w : getServer().getWorlds()) saveCenotaphList(w.getName());
		if (dynmapEnable && dynmap != null) dynThread.cenotaphLayer.cleanup();
		getServer().getScheduler().cancelTasks(this);
	}
	private String[] loadSign() {
		String[] msg = signMessage;
		msg[0] = signMessage[0];
		msg[1] = signMessage[1];
		msg[2] = signMessage[2];
		msg[3] = signMessage[3];
		return msg;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return commandExec.onCommand(sender, command, label, args);
	}

	/*
	 * Check if a plugin is loaded/enabled. Returns the plugin and print message to console if so, returns null otherwise
	 */
	private Plugin loadPlugin(String p) {
		Plugin plugin = pm.getPlugin(p);
		if (plugin != null && plugin.isEnabled()) {
			log.log(Level.INFO, "[Cenotaph] Using {0} (v{1})", new Object[]{plugin.getDescription().getName(), plugin.getDescription().getVersion()});
			return plugin;
		}
		return null;
	}

	public void deactivateLockette(TombBlock tBlock) {
		if (tBlock.getLocketteSign() == null) return;
		tBlock.getLocketteSign().getBlock().setType(Material.AIR);
		tBlock.removeLocketteSign();
	}

	public void removeTomb(TombBlock tBlock, boolean removeList) {
		if (tBlock == null) return;

		tombBlockList.remove(tBlock.getBlock().getLocation());
		if (tBlock.getLBlock() != null) tombBlockList.remove(tBlock.getLBlock().getLocation());
		if (tBlock.getSign() != null) tombBlockList.remove(tBlock.getSign().getLocation());

		// Remove just this tomb from tombList
		ArrayList<TombBlock> tList = playerTombList.get(tBlock.getOwner());
		if (tList != null) {
			tList.remove(tBlock);
			if (tList.isEmpty()) {
				playerTombList.remove(tBlock.getOwner());
			}
		}

		if (removeList)
			tombList.remove(tBlock);

		if (tBlock.getBlock() != null)
			saveCenotaphList(tBlock.getBlock().getWorld().getName());
	}


	public double getYawTo(Location from, Location to) {
			final int distX = to.getBlockX() - from.getBlockX();
			final int distZ = to.getBlockZ() - from.getBlockZ();
			double degrees = Math.toDegrees(Math.atan2(-distX, distZ));
			degrees += 180;
		return degrees;
	}

	/**
	 * Converts a rotation to a cardinal direction name.
	 * Author: sk89q - Original function from CommandBook plugin
	 * @param rot
	 * @return
	 */
	public static String getDirection(double rot) {
		if (0 <= rot && rot < 22.5) {
			return "North";
		} else if (22.5 <= rot && rot < 67.5) {
			return "Northeast";
		} else if (67.5 <= rot && rot < 112.5) {
			return "East";
		} else if (112.5 <= rot && rot < 157.5) {
			return "Southeast";
		} else if (157.5 <= rot && rot < 202.5) {
			return "South";
		} else if (202.5 <= rot && rot < 247.5) {
			return "Southwest";
		} else if (247.5 <= rot && rot < 292.5) {
			return "West";
		} else if (292.5 <= rot && rot < 337.5) {
			return "Northwest";
		} else if (337.5 <= rot && rot < 360.0) {
			return "North";
		} else {
			return null;
		}
	}

	public String convertTime(int s) {
		String formatted = Integer.toString(s);
		if (s >= 86400) {
			formatted = String.format("%dd %d:%02d:%02d", s/86400, (s%86400)/3600, (s%3600)/60, s%60);
		}
		else if (s >= 3600) {
			formatted = String.format("%d:%02d:%02d", s/3600, (s%3600)/60, (s%60));
		}
		else if (s > 60) {
			formatted = String.format("%02d:%02d", s/60, s%60);
		}
		return formatted;
	}

	public void sendMessage(Player player, String message) {
		player.sendMessage(ChatColor.GOLD + "[Cenotaph] " + ChatColor.WHITE + message);
	}

	public void destroyCenotaph(Location loc) {
		destroyCenotaph(tombBlockList.get(loc));
	}
	public void destroyCenotaph(TombBlock tBlock) {
		if (tBlock.getBlock().getChunk().load() == false) {
			log.log(Level.SEVERE, "[Cenotaph] Error loading world chunk trying to remove cenotaph at {0},{1},{2} owned by {3}.", new Object[]{tBlock.getBlock().getX(), tBlock.getBlock().getY(), tBlock.getBlock().getZ(), tBlock.getOwner()});
			return;
		}
		if (tBlock.getSign() != null) tBlock.getSign().setType(Material.AIR);
		deactivateLockette(tBlock);

		tBlock.getBlock().setType(Material.AIR);
		if (tBlock.getLBlock() != null) tBlock.getLBlock().setType(Material.AIR);

		removeTomb(tBlock, true);

		Player p = getServer().getPlayer(tBlock.getOwner());
		if (p != null) sendMessage(p, "Tu Cenopath ha sido elimiando.");
	}

	public HashMap<String, ArrayList<TombBlock>> getCenotaphList() {
		return playerTombList;
	}
}
