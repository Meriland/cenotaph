package com.MoofIT.Minecraft.Cenotaph;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;

public class TombBlock {
	private final Block block;
	private final Block lBlock;
	private final Block sign;
	private Sign LocketteSign;
	private final long time;
	private final String owner;
	private final int ownerLevel;

	TombBlock(Block block, Block lBlock, Block sign, String owner, int ownerLevel, long time) {
		this.block = block;
		this.lBlock = lBlock;
		this.sign = sign;
		this.owner = owner;
		this.ownerLevel = ownerLevel;
		this.time = time;
	}

	long getTime() {
		return time;
	}
	Block getBlock() {
		return block;
	}
	Block getLBlock() {
		return lBlock;
	}
	Block getSign() {
		return sign;
	}
	Sign getLocketteSign() {
		return LocketteSign;
	}
	String getOwner() {
		return owner;
	}
	int getOwnerLevel() {
		return ownerLevel;
	}
	void setLocketteSign(Sign sign) {
		this.LocketteSign = sign;
	}
	void removeLocketteSign() {
		this.LocketteSign = null;
	}
}
